from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from core.base import base_dataset
from core.utilities import dataset_utilities
from core.utilities import image_utilities
import numpy as np
import os
import pickle
import torch

class DatasetModel(base_dataset.Dataset):
    def __init__(self, configObject):
        super(DatasetModel, self).__init__(configObject)
        self.__name__ = 'Relaxometry Dataset'
        self.args = configObject.args
        self.data = []
        self.idx_control = -1
        self.args.task.tau = np.divide(self.args.task.tau,1000)
        if self.args.task.sigmaNoise >= 0:
            self.sigma = self.args.task.sigmaNoise
        elif self.args.task.sigmaNoise < 0:
            self.sigma = None #TODO: Make it random selection - Create iterator over random values for SNR

    def prepare_anatomical_maps(self, idx):
        self.data[idx] = np.reshape(self.data[idx], (362, 434, 362))

    def set_parameter_values(self):
        if self.args.task.signalModel == 'looklocker':
            tp = {"csf": 3.5, "gm": 1.4, "wm": 0.78, "fat": 0.42, 
            "muscle": 1.2, "muscle_skin": 1.23, "skull": 0.4, "vessels": 1.98, 
            "connect": 0.9, "dura": 0.9, "bone_marrow": 0.58}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
            
        elif self.args.task.signalModel == 'fse':
            tp = {"csf": 2, "gm": 0.11, "wm": 0.08, "fat": 0.07, 
            "muscle": 0.05, "muscle_skin": 0.05, "skull": 0.03, "vessels": 0.275, 
            "connect": 0.08, "dura": 0.07, "bone_marrow": 0.05}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
        self.parameters = [tp, pd]

    def generate_simulated_training_label(self, idx):
        data_ = self.data[idx]
        self.args.task.pngr = dataset_utilities.get_rand_seed(self.args.task.useRandomSeed)
        if self.args.task.usePatches:
            patch_extractor = dataset_utilities.ExtractPatch(self.args)
            data_ = patch_extractor.get_patch(data_)

        slice_index = int(self.args.task.pngr.uniform(10, int(data_.shape[-1])-10))
        data_ = data_[..., slice_index]

        #TODO: initializeParameters comes from parameter number in signal model

        qMap = np.zeros_like(data_, dtype=float)
        pdMap = np.zeros_like(data_, dtype=float)
        mask = np.zeros_like(data_, dtype=float)
        mask[data_ > 0] = 1

        self.set_parameter_values()
        relaxation_param = list(self.parameters[0].items())
        proton_density = list(self.parameters[1].items())

        if self.args.task.signalModel == 'fse':
            std_tissue = 0.03 
        elif self.args.task.signalModel == 'looklocker':
            std_tissue = 0.3
        if len(np.shape(data_)) > 2:
            for p in range(len(data_)):
                self.args.task.pngr = dataset_utilities.get_rand_seed(self.args.task.useRandomSeed)
                for ind in np.unique(data_):
                    if ind>0:
                        qMap[p, data_[p] == ind] = np.abs(self.args.task.pngr.normal(relaxation_param[ind-1][1], std_tissue))
                        pdMap[p, data_[p] == ind] = np.abs(self.args.task.pngr.normal(proton_density[ind-1][1], 0.3))+0.1
        else:
            for ind in np.unique(data_):
                if ind > 0 :    
                    qMap[data_ == ind] = np.abs(self.args.task.pngr.normal(relaxation_param[ind-1][1], std_tissue))
                    pdMap[data_ == ind] = np.abs(self.args.task.pngr.normal(proton_density[ind-1][1], 0.3))+0.1
        
        qMap = np.stack(qMap)
        pdMap = np.stack(pdMap)

        if self.args.task.signalModel == 'looklocker':
            bMap = []
            for pi, p_map in enumerate(pdMap):
                self.args.task.pngr = dataset_utilities.get_rand_seed(self.args.task.useRandomSeed)
                abs_bMap = np.abs(self.args.task.pngr.normal(2.0, 0.0, np.shape(p_map)))
                bMap_ = (2 - np.abs(abs_bMap - 2))*mask[pi]
                bMap.append(bMap_)
            bMap = np.stack(bMap)
            kappa = [pdMap, bMap, qMap]
        elif self.args.task.signalModel == 'fse':
            kappa = [pdMap, qMap]
        
        kappa = dataset_utilities.apply_gt_noise(kappa, self.args.task.pngr, self.args)
        kappa = dataset_utilities.smooth_maps(kappa, self.args)

        if self.args.task.simulateArtefacts:
            kappa = dataset_utilities.add_artefacts(kappa)

        self.training_label = torch.from_numpy(np.abs(kappa)).type(torch.FloatTensor).to(self.args.engine.device)
        mask = np.expand_dims(mask, 1).repeat(self.training_label.size()[1],1)
        self.mask = torch.from_numpy(mask).type(torch.FloatTensor).to(self.args.engine.device)

    def generate_simulated_training_signal(self):
        weightedSeries = []
        for label_patch in self.training_label:
            weighted_images = self.args.engine.signal_model.forwardModel(label_patch, self.args.task.tau)
            weighted_images = weighted_images.type(torch.FloatTensor).to(self.args.engine.device)
            if not self.sigma:
                s = torch.distributions.log_normal.LogNormal(0.0, 1)
                self.sigma = s.sample()/60

            weighted_images_noisy = self.args.engine.likelihood_model.applyNoise(weighted_images, self.sigma)
            weightedSeries.append(weighted_images_noisy)
        self.training_signal = torch.stack(weightedSeries)

    def get_existing_data(self, idx):
        if not self.args.task.preLoadData:
            data_ = self.data[0]
        else:
            data_ = self.data[idx]
        
        self.training_signal = torch.Tensor(data_['weighted_series']).type(torch.FloatTensor)
        self.training_label = torch.Tensor(data_['label']).type(torch.FloatTensor) if 'label' in data_ else []
        self.mask = torch.Tensor(data_['mask']).type(torch.FloatTensor) if 'mask' in data_ else []

        if self.training_signal.dim() > 3:
            self.training_signal = self.training_signal[...,8]

        self.args.engine.signal_model.setTau(torch.Tensor(data_['echo_times']).type(torch.FloatTensor))

    def get_label(self, idx):
        if self.args.task.useSimulatedData:
            self.prepare_anatomical_maps(idx)
            self.generate_simulated_training_label(idx)
        else:
            if not self.args.task.preLoadData:
                self.load_file(idx)
            self.get_existing_data(idx)
        return self.training_label, self.mask

    def get_signal(self, idx):
        if self.args.task.useSimulatedData:
            self.generate_simulated_training_signal()
        return self.training_signal


from . import *
from os.path import dirname, realpath

filepath = realpath(__file__)
parent_dir = dirname(filepath)
rec = 4 # This is always related to where the module is compared to the root of EMCqMRI. In this case, 4 folders down.
for _ in range(rec):
    parent_dir = dirname(parent_dir)

import sys
sys.path.insert(0,parent_dir)
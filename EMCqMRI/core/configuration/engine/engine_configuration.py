from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
from EMCqMRI.core.utilities import configuration_utilities


class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):
        group_task = self.configObject.parser.add_argument_group('task_config')
        group_task.add_argument('-optimizer', required=self.required, type=float, help='Optimizer')
        group_task.add_argument('-learningRate', required=self.required, type=float, help='Learning rate for network training (default:0.0001)')
        group_task.add_argument('-batchSize', required=self.required, type=int, help='Size of training batch')
        group_task.add_argument('-epochs', required=self.required, type=int, help='Number of training epochs. If mode=testing, epochs=1')
        group_task.add_argument('-lossFunction', required=self.required, type=str, help='Cost function for network training')
        group_task.add_argument('-loadCheckpoint', required=self.required, type=configuration_utilities.str2bool, help='Load existing checkpoint in -loadCheckpointPath')
        group_task.add_argument('-loadCheckpointPath', required=self.required, type=str, help='Path + filename to existing model checkpoint')
        group_task.add_argument('-trainingDataPath', required=self.required, type=str, help='Path to training data')
        group_task.add_argument('-testingDataPath', required=self.required, type=str, help='Path to testing data')
        group_task.add_argument('-validationDataPath', required=self.required, type=str, help='Path to validation data')
        group_task.add_argument('-datasetModel', required=self.required, type=str, help='Name of dataset model to generate or load data')
        group_task.add_argument('-saveResults', required=self.required, type=configuration_utilities.str2bool, help='Save estimates at each epoch at the specified -resultsPath')
        group_task.add_argument('-saveResultsPath', required=self.required, type=str, help='Path to save model output data. Required if -saveResults is True')
        group_task.add_argument('-saveCheckpoint', required=self.required, type=configuration_utilities.str2bool, help='Save a model checkpoint to the path specified in -saveCheckpointPath')
        group_task.add_argument('-saveCheckpointPath', required=self.required, type=str, help='Path to save model checkpoint')
        group_task.add_argument('-sulfixCheckpoint', required=self.required, type=str, help='Suffix to add to save checkpoint file')
        group_task.add_argument('-useCUDA', required=self.required, type=configuration_utilities.str2bool, help='If True, use GPU. If False or not specified, use CPU')
        group_task.add_argument('-runBenchmark', required=self.required, type=bool, help='if True (default), run GPU optimization benchmark')
        group_task.add_argument('-inferenceModel', required=self.required, type=str, help='Method for parameter estimation')
        group_task.add_argument('-usePatchesAsBatches', required=self.required, type=float, help='If True, it sets (internally) the dataloader batchsize to 1 and uses the patches to create batches of size args.engine.batchSize')
        

        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration
        

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
from EMCqMRI.core.utilities import configuration_utilities



class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):       
        group_task = self.configObject.parser.add_argument_group('method_config')
        group_task.add_argument('-numberOfIterations', required=self.required, type=int, help='Number of iterations of optimization')
        
        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from EMCqMRI.core.utilities import configuration_utilities


class Configuration(object):
    def __init__(self, configObject):
        self.configObject = configObject
        self.required = False

    def parse_configuration(self):
        group_task = self.configObject.parser.add_argument_group('relaxometry_config')
        group_task.add_argument('-signalModel', required=self.required, type=str, help='Forward model (T1, T2)')
        group_task.add_argument('-useSimulatedData', required=self.required, type=configuration_utilities.str2bool, help='Use synthetic data')
        group_task.add_argument('-likelihoodModel', required=self.required, type=str, help='Likelihood function (Gaussian, Rician)')
        group_task.add_argument('-sigmaNoise', required=self.required, type=float, help='Standard deviation of simulated acquisition noise. If any value is given, all training samples will be produced with this SD. If no value is given, random SDs will be used')
        group_task.add_argument('-simulateArtefacts', required=self.required, type=configuration_utilities.str2bool, help='If True, signal artefacts will be added to ground-truth maps')
        group_task.add_argument('-useRandomSeed', required=self.required, type=configuration_utilities.str2bool, help='If True, all values generated via random sampling will be generated from the same seed')
        group_task.add_argument('-tau', required=self.required, type=float, help='Values in miliseconds. If not specified, default values are used')
        group_task.add_argument('-preLoadData', required=self.required, type=configuration_utilities.str2bool, help='If True, preload all data in the dataset path')
        group_task.add_argument('-usePatches', required=self.required, type=configuration_utilities.str2bool, help='If True, 2d patches will be extracted from the data')
        group_task.add_argument('-patchSize', required=self.required, type=float, help='Size of the patch, in pixels')
        
        
        config_args, _ = self.configObject.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)
        return configuration

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.append('../')
import os

import argparse
import importlib
import logging
import ntpath
from EMCqMRI.core.utilities import configuration_utilities

class Configuration(object):
    def __init__(self, mode):
        self.parser = argparse.ArgumentParser(description='Global configuration for inference models')
        self.group_global = self.parser.add_argument_group('global_config')
        
        configuration_types = {'task': configuration_utilities.dict_to_attr(),
                                'inference': configuration_utilities.dict_to_attr(),
                                'engine': configuration_utilities.dict_to_attr(),
                                'core': configuration_utilities.dict_to_attr()
                                }

        self.args = configuration_utilities.qMRIopts_to_attr()
        self.args.__update__(**configuration_types)

        self.args.core.mode = mode
        self.set_global_config()

    def set_global_config(self):
        self.group_global.add_argument('-configurationFile', type=str, help='Path to pll_configuration file. If not provided, command line arguments are required')
        self.temp_args, _ = self.parser.parse_known_args()

        if os.path.isfile(self.temp_args.configurationFile):
            configuration_ = configuration_utilities.parse_json_configuration(self.temp_args.configurationFile)
            self.args.core.__update__(**configuration_)
            self.required = False
        else:
            self.required = True
        cmd_configuration = self.parse_command_line_configuration()
        self.args.core.__update__(**cmd_configuration)
        config_parser = configuration_utilities.ParseConfiguration()

        ## Set configuration for training/testing routines
        engine_configuration = config_parser.parse_configuration(self.args.core.engineConfigurationFile)
        self.args.engine.__update__(**engine_configuration)
        if self.args.core.allowCMDoverride:
            config_ = self.parse_imported_cmd_configuration(self.args.core.engineConfigurationFile)
            if config_:
                self.args.engine.__update__(**config_)

        ## Set configuration of the task
        task_configuration = config_parser.parse_configuration(self.args.core.taskConfigurationFile)
        self.args.task.__update__(**task_configuration)
        if self.args.core.allowCMDoverride:
            config_ = self.parse_imported_cmd_configuration(self.args.core.taskConfigurationFile)
            if config_:
                self.args.task.__update__(**config_)

        ## Set configuration of inference method
        method_configuration = config_parser.parse_configuration(self.args.core.inferenceModelConfigurationFile)
        self.args.inference.__update__(**method_configuration)
        if self.args.core.allowCMDoverride:
            config_ = self.parse_imported_cmd_configuration(self.args.core.inferenceModelConfigurationFile)
            if config_:
                self.args.inference.__update__(**config_)

        if self.args.core.displayConfigurations:
            print("")
            logging.info("*"*40)
            logging.info("CURRENT CONFIGURATIONS:")
            logging.info(self.args.__list_attr__())
            logging.info("*"*40)
            print("")

    def parse_imported_cmd_configuration(self, configFile):
        absolute_path = ntpath.abspath(configFile)
        norm_path = ntpath.normpath(ntpath.splitext(absolute_path)[0]).split('\\')
        module_name = norm_path[-1]
        module_path = '/'.join(ntpath.splitext(absolute_path)[0].split('\\')) + '.py'
        try:
            spec = importlib.util.spec_from_file_location(module_name, module_path)
            module = importlib.util.module_from_spec(spec)
            sys.modules[spec.name] = module 
            spec.loader.exec_module(module)
        except FileNotFoundError:
            print("")
            logging.warning("Could not find configuration file for module {}, at location {}".format(module_name, module_path))
            logging.info("Skipping parsing of command line arguments for {}".format(module_name))
            print("")
            return None

        cmd_config = module.Configuration(self)
        parsed_config = cmd_config.parse_configuration()
        return parsed_config
            
    def parse_command_line_configuration(self):
        self.group_global.add_argument('-engineConfigurationFile', required=self.required, type=str, help='Path to configuration file of training/testing engine')
        self.group_global.add_argument('-inferenceModelConfigurationFile', required=self.required, type=str, help='Path to configuration file of inference model')
        self.group_global.add_argument('-taskConfigurationFile', required=self.required, type=str, help='Path to configuration file of task')
        self.group_global.add_argument('-allowCMDoverride', required=self.required, type=configuration_utilities.str2bool, help='If True, it looks for a python file with the same name as json/txt configuration file and parses command line information.')
        self.group_global.add_argument('-task', required=self.required, type=str, help='Name of the task (e.g. relaxometry')
        self.group_global.add_argument('-displayConfigurations', required=self.required, type=str, help='If True, log all configuration settings')
        
        config_args, _ = self.parser.parse_known_args()
        configuration = configuration_utilities.convert_argparse_to_attr(config_args)

        return configuration

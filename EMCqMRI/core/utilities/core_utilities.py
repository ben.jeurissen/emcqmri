from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import importlib
import logging
import progress.bar


def load_ext_module(module_link, module_name, configObject):
    module = importlib.import_module(module_link)
    loaded_module = str_to_class(module, module_name.capitalize())
    return loaded_module(configObject)


def str_to_class(module, classname):
    return getattr(module, classname)
    

def alternateTrainingState(dataloader, batchSize):
    while True:
        yield {'training': dataloader[0], 'batchSize': batchSize}
        yield {'validation': dataloader[1], 'batchSize': 1}


def batch_iterator(numberOfPatches, batchSize):
    start_index = 0
    while True:
        data = yield
        end_index = numberOfPatches if start_index + batchSize > numberOfPatches else start_index + batchSize
        batched_data = []
        for dat in data:
            batched_data.append(dat[start_index:end_index])
        yield batched_data, [end_index, start_index]
        if end_index != numberOfPatches:
            start_index += batchSize


def get_batch(data, args, batch_generator):
    data_ = []
    data_.append(data[0][0])
    args.engine.filename = data[1][0]

    if len(data)>2:
        if isinstance(data[2],list):
            for l_data in data[2]:
                data_.append(l_data[0])
        else:
            data_.append(data[2][0])
    if len(data)>3:
        data_.append(data[3][0])

    batched_data, indexes = batch_generator.send(data_)
    args.engine.batchSizeIter = indexes[0] - indexes[1]

    batched_data_ = []
    batched_data_.append(batched_data[0])
    if len(batched_data)>1:
        if isinstance(data[2],list):
            batched_data_.append([batched_data[1],batched_data[2]])
            batched_data_.append(batched_data[3])
        else:
            batched_data_.append(batched_data[1])
            batched_data_.append(batched_data[2])

    return batched_data_, indexes


class ProgressWrapper(progress.bar.FillingSquaresBar):
    def __init__(self, *args, **kwargs):
        super(ProgressWrapper, self).__init__(*args, **kwargs)
    
    def update(self):
        filled_length = int(self.width * self.progress)
        empty_length = self.width - filled_length
        message = self.message % self
        bar_ = self.fill * filled_length
        empty = self.empty_fill * empty_length
        suffix = self.suffix % self
        self.loss = '  Loss:  '+str(self.loss_)
        self.subs = 'Subject: '+str(self.it_sub)+'/'+str(self.total_subs) + '  '
        line = ''.join([self.subs, message, self.bar_prefix, bar_, empty, self.bar_suffix,
                        suffix, self.loss])
        self.writeln(line)

    def set_total_sub(self, total_subs):
        self.total_subs = total_subs

    def set_max(self, max_):
        self.max = max_

    def reset(self):
        self.index = 0
        self.loss_ = 0
        self.it_sub = 0

    def update_ext_par(self, loss, it_sub):
        self.loss_ = loss
        self.it_sub = it_sub


def ProgressBarWrap(func):
    bar = ProgressWrapper('', max=1, suffix='%(percent)d%%', loss=10)
    def wrapper(self, loss, args):
        bar.set_max(args.inference.inferenceSteps)
        bar.set_total_sub(args.engine.lenDataset)
        bar.update_ext_par(loss, args.engine.iter)
        bar.next()
        
        if bar.index == args.inference.inferenceSteps:
            bar.finish()
            bar.reset()
    return wrapper



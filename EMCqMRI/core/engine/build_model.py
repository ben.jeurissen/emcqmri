from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../..')

from ..utilities import checkpoint_utilities
from ..utilities import core_utilities
from . import data_loader
import importlib
import logging
import torch
import torch.nn as nn



def make(configObject):
    """Links all configurations to internal modules, which are dinamically imported.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - useCUDA
            - device
            - runBenchmark
            - likelihoodModel
            - signalModel
            - inferenceModel
            - task
            - optimizer
            - learningRate
            - lossFunction
            - loadCheckpoint
            - loadCheckpointPath

    Returns:
        [type: Configuration]: Updated configObject containing all imported Python modules
    """

    if configObject.args.engine.useCUDA:
        if torch.cuda.is_available():
            configObject.args.engine.device = torch.device("cuda:0")
            logging.info("Using CUDA")
        else:
            logging.warn("User selected runtime with CUDA, but CUDA is not available. Running on CPU instead.")
            configObject.args.engine.device = torch.device("cpu")
    else:
        configObject.args.engine.device = torch.device("cpu")
        logging.info("Using CPU")
    torch.backends.cudnn.benchmark = configObject.args.engine.runBenchmark


    ###############################################################
    ###############################################################
    # VALIDATED LIKELIHOOD MODELS
    # OPTIONS: {'gaussian', 'rician'}
    if not hasattr(configObject.args.engine, 'likelihood_model'):
        module_name = configObject.args.task.likelihoodModel
        configObject.args.engine.likelihood_model = core_utilities.load_ext_module("EMCqMRI.core.modules.likelihood_model." + 
                                                                             module_name + "." + 
                                                                             module_name, module_name, configObject)


    ################################################################
    ################################################################
    # VALIDATED SIGNAL MODELS
    # OPTIONS: {'looklocker', 'fse'}
    if not hasattr(configObject.args.engine, 'signal_model'):
        module_name = configObject.args.task.signalModel
        configObject.args.engine.signal_model = core_utilities.load_ext_module("EMCqMRI.core.modules.signal_model." + 
                                                                             module_name + "." + 
                                                                             module_name, module_name, configObject)

    ################################################################
    ################################################################
    # VALIDATED INFERENCE MODELS
    # OPTIONS: {'rim', 'mle', 'resnet'}
    if not hasattr(configObject.args.engine, 'inference_model'):
        module_name = configObject.args.engine.inferenceModel
        configObject.args.engine.inference_model = core_utilities.load_ext_module("EMCqMRI.core.modules.inference_model." + 
                                                                             module_name + "." + 
                                                                             module_name, module_name, configObject)        


    ################################################################
    ################################################################
    # VALIDATED DATASETS
    # OPTIONS: {'relaxometry'}
    if not hasattr(configObject.args.engine, 'dataset'):
        module_name = configObject.args.core.task
        module = importlib.import_module("EMCqMRI.core.modules.dataset." + module_name + "." + module_name + '_dataset')
        configObject.args.engine.dataset = module.DatasetModel(configObject)


    ################################################################
    ################################################################
    # OPTIMIZER
    if isinstance(configObject.args.engine.inference_model, nn.Module):
        optimizer_options = ['ADAM', 'AdaDelta', 'RMSProp', 'SGD']
        if configObject.args.engine.optimizer == optimizer_options[0]:
            configObject.args.engine.optimizer = torch.optim.Adam(configObject.args.engine.inference_model.parameters(), lr=configObject.args.engine.learningRate)
        elif configObject.args.engine.optimizer == optimizer_options[1]:
            configObject.args.engine.optimizer = torch.optim.Adadelta(configObject.args.engine.inference_model.parameters(), lr=configObject.args.engine.learningRate)
        elif configObject.args.engine.optimizer == optimizer_options[2]:
            configObject.args.engine.optimizer = torch.optim.RMSprop(configObject.args.engine.inference_model.parameters(), lr=configObject.args.engine.learningRate)
        elif configObject.args.engine.optimizer == optimizer_options[3]:
            configObject.args.engine.optimizer = torch.optim.SGD(configObject.args.engine.inference_model.parameters(), lr=configObject.args.engine.learningRate)
        else:
            logging.warn("Optimizer '{}' not supported. Either choose between ADAM, AdaDelta, RMSProp, SGD or implement your own optimizer".format(configObject.args.engine.lossFunction))

    
    ################################################################
    ################################################################
    # OPTIMIZER COST FUNCTION
    costFun_options = ['MSE', 'L1', 'NLLLoss', 'SmoothL1']
    if configObject.args.engine.lossFunction == costFun_options[0]:
        configObject.args.engine.objective_fun = torch.nn.MSELoss(reduction='mean')
    if configObject.args.engine.lossFunction == costFun_options[1]:
        configObject.args.engine.objective_fun = torch.nn.L1Loss(reduction='mean')
    if configObject.args.engine.lossFunction == costFun_options[2]:
        configObject.args.engine.objective_fun = torch.nn.NLLLoss(reduction='mean')
    if configObject.args.engine.lossFunction == costFun_options[3]:
        configObject.args.engine.objective_fun = torch.nn.SmoothL1Loss(reduction='mean')
    else:
        logging.warn("Loss function '{}' not supported. Either choose between MSE, L1, NLLLoss, SmoothL1 or implement your own loss function".format(configObject.args.engine.lossFunction))


    ################################################################
    ################################################################
    # DEFINITION OF DATALOADER 
    dataloader = data_loader.create_dataloader(configObject)
    configObject.args.engine.dataloader = dataloader

    ################################################################
    ################################################################
    # LOAD PRE-TRAINED MODEL
    if isinstance(configObject.args.engine.inference_model, nn.Module):
        configObject.args.engine.inference_model.to(device=configObject.args.engine.device)
        if configObject.args.engine.loadCheckpoint:
            logging.info("Loading model checkpoint from {}.".format(configObject.args.engine.loadCheckpointPath))
            checkpoint_utilities.load(configObject)

    print("")
    logging.info("*"*40)
    logging.info("MODEL ARCHITECTURE:")
    logging.info(configObject.args.engine.inference_model)
    logging.info("*"*40)
    print("")

    print("")
    logging.info("*"*40)
    logging.info("OPTIMIZER:")
    logging.info(configObject.args.engine.optimizer)
    logging.info("*"*40)
    print("")

    return configObject
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from ..utilities import image_utilities
from ..utilities import core_utilities
from ..utilities import checkpoint_utilities

import logging
import numpy as np

import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')

class Train(object):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save intermediate results and training checkpoints
        and/or display the intermediate results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - epochs
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - numberOfPatches
            - objective_fun
            - optimizer
            - saveResults
            - saveResultsPath
            - saveCheckpoint
            - saveCheckpointPath
            - usePatchesAsBatches
    """
    def __init__(self, configObject):
        self.args = configObject.args
        if isinstance(self.args.engine.dataloader, list): # It means that validation dataset is being used.
            self.dataloader_state = core_utilities.alternateTrainingState(self.args.engine.dataloader, self.args.engine.batchSize)
            state = next(self.dataloader_state)
            self.dataloader = list(state.values())[0]
            self.args.engine.stateName = list(state.keys())[0]
        else:
            self.args.engine.stateName = 'training'
            self.dataloader = self.args.engine.dataloader
        self.epoch = 0
        self.args.engine.inference_model.train()

    def execute(self, task = 'relaxometry'):
        for ep in range(self.args.engine.epochs):
            logging.info('Running {}; Epoch {}'.format(self.args.engine.stateName, self.epoch))
            logging.info('*'*50)
            self.args.engine.lenDataset = len(self.dataloader)
            for sample, data in enumerate(self.dataloader):
                self.args.engine.test_sample = sample
                self.__initialize_batch_generator__()
                if self.args.engine.usePatchesAsBatches:
                    for _ in range(self.patch_iterations):
                        #TODO Include handling of list of labels within the get_batch...
                        batched_data, indexes = self.__get_batch__(data)
                        processed_data, loss = self.__run__(batched_data)
                        self.__log_error__(self.epoch, sample, indexes, loss)
                    # image_utilities.imagebrowse_slider(processed_data['estimated'][0])
                else:
                    processed_data, loss = self.__run__(data)
                    self.__log_error__(self.epoch, sample, 1, loss)
            self.__end_epoch__(processed_data)

    def __end_epoch__(self, processed_data, ):
        if self.args.engine.stateName == 'training':
            self.epoch += 1
        if self.args.engine.saveResults:
            image_utilities.saveItermediateResults(processed_data, self.args, self.epoch)
        if self.args.engine.saveCheckpoint:
            checkpoint_utilities.save(self.args, self.epoch, self.args.engine.inference_model) 
            
        if isinstance(self.args.engine.dataloader, list):
            state = next(self.dataloader_state)
            self.dataloader = list(state.values())[0]
            self.args.engine.stateName = list(state.keys())[0]
            self.args.engine.inference_model.train() if self.args.engine.stateName == 'training' else self.args.engine.inference_model.eval()

    def __get_batch__(self, input_data):
        next(self.batch_generator)
        batched_data, indexes = core_utilities.get_batch(input_data, self.args, self.batch_generator)
        signal = batched_data[0]
        if len(batched_data) > 1:
            label = batched_data[1]
            mask = batched_data[2]
            data = [signal, label, mask]
        else:
            data = [signal]
        return data, indexes

    def __log_error__(self, epoch, i, indexes, loss):
        logging.info("Epoch: {}, State: {}, Subject: {}/{}, Patches {}/{}, Loss: {} ".format(epoch, 
                            self.args.engine.stateName, i, self.dataloader.__len__(), 
                            indexes[0], self.args.task.numberOfPatches, loss))

    def __initialize_batch_generator__(self):
            self.batch_generator = core_utilities.batch_iterator(self.args.task.numberOfPatches, self.args.engine.batchSize)
            self.patch_iterations = int(np.ceil(self.args.task.numberOfPatches/self.args.engine.batchSize))

    def __run__(self, data_):
        signal = data_[0]
        if len(data_) > 1:
            label = data_[1]
            mask = data_[2]

        if self.args.engine.inference_model.__require_initial_guess__:
            initial_kappa = self.args.engine.signal_model.initializeParameters(signal)
            inputs = [signal, initial_kappa]
        else:
            inputs = [signal]

        estimate = self.args.engine.inference_model.forward(inputs)
        
        loss_batch = 0
        if isinstance(estimate, list):
            if isinstance(label, list):
                loss_iteration = []
                for estimate_iteration in estimate:
                    loss_ = [self.args.engine.objective_fun(estim.squeeze(0), lab.squeeze(0).float()) for estim, lab in zip(estimate_iteration, label)]
                    loss_iteration.append(sum(loss_)/len(loss_))
                loss = sum(loss_iteration)/len(loss_iteration)
            else:
                loss = [self.args.engine.objective_fun(e*mask, label*mask) for e in estimate]
                loss = sum(loss) / len(loss)
        else:
            if isinstance(label, list):
                pass
            else:
                loss = self.args.engine.objective_fun(estimate*mask, label*mask)

        if self.args.engine.stateName == 'training':
            loss.backward()
            self.args.engine.optimizer.step()
            self.args.engine.optimizer.zero_grad()

        last_estimate = estimate[-1]
        processed_data = {}

        if isinstance(label, list):
            processed_data['estimated'] = [l_e[0].cpu().detach().numpy() for l_e in last_estimate]
            if len(data_) > 1:
                processed_data['labels'] = [l[0].cpu().detach().numpy() for l in label]
                processed_data['mask'] = mask[0].cpu().detach().numpy()
        return processed_data, loss

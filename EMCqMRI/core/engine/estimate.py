from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from ..utilities import image_utilities
import logging
import torch
import torch.nn as nn

logging.basicConfig(level=logging.INFO)
logging.info('Setting up environment...')

class Infer(object):
    """Performs a forward pass through a given inference model.
        Depending on the options set, it might save intermediate results and training checkpoints
        and/or display the intermediate results.

    Args:
        configObject ([type: Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - epochs
            - inference_model
            - dataloader
            - signal_model (if configObject.args.inference_model.__require_initial_guess__ == True)
            - numberOfPatches
            - objective_fun
            - optimizer
            - saveResults
            - saveResultsPath
            - saveCheckpoint
            - saveCheckpointPath
            - usePatchesAsBatches
    """
    def __init__(self, configObject):
        self.args = configObject.args

    def execute(self, return_result = False):
        self.args.engine.stateName = 'testing'
        self.dataloader = self.args.engine.dataloader
        self.args.engine.batchSizeIter = 1

        if isinstance(self.args.engine.inference_model, nn.Module):
            self.args.engine.inference_model.eval()

        epoch = '_estimating_'
        logging.info('Running {}'.format(self.args.engine.stateName))
        logging.info('*'*50)
        self.args.engine.lenDataset = len(self.dataloader)
        for i, data in enumerate(self.dataloader):
            self.args.engine.iter = i
            processed_data = self.__run__(data)

            if self.args.engine.saveResults:
                self.args.engine.filename = data[1][0]
                filename_ = self.args.engine.filename + 'patch_' + str(i)
                image_utilities.saveDataPickle(processed_data, self.args.engine.saveResultsPath, filename_)

        if return_result:
            return processed_data

    def __run__(self, data_):
        signal = data_[0]
        if len(data_) > 1:
            label = data_[2]
            mask = data_[3]

        if self.args.engine.inference_model.__require_initial_guess__:
            initial_kappa = self.args.engine.signal_model.initializeParameters(signal)
            inputs = [signal, initial_kappa]
        else:
            inputs = [signal]

        estimate = self.args.engine.inference_model.forward(inputs)
        
        processed_data = {}
        if isinstance(label, list):
            processed_data['estimated'] = [l_e[0].cpu().detach().numpy() for l_e in estimate]
            if len(data_) > 1:
                processed_data['labels'] = [l[0].cpu().detach().numpy() for l in label]
                processed_data['mask'] = mask[0].cpu().detach().numpy()
        else:
            processed_data['estimated'] = estimate[0].squeeze().cpu().detach().numpy()
            if len(data_) > 2:
                processed_data['labels'] = label[0].cpu().detach().numpy()
                processed_data['mask'] = mask[0].cpu().detach().numpy()

        return processed_data

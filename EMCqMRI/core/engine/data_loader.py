from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from pathlib import Path


class SampleDatabase(Dataset):
    """ Wrapper function for custom dataset subclasses.
    """
    def __init__(self, configObject, path=[]):
        self.dataset = configObject.args.engine.dataset
        self.dataset.set_data_path(path)
        self.dataset.index_files()
        if configObject.args.task.preLoadData:
            self.dataset.load_folder()
        #else: the base_dataset class has a method load_file(idx) that loads one file per iteration.
        #load_file(idx) should be called within get_label(idx) and get_signal(idx) within your custom dataset

    def __len__(self):
        return self.dataset.get_length()

    def __getitem__(self, idx):
        label, mask = self.dataset.get_label(idx)
        filename = Path(self.dataset.filename_list[idx]).stem
        signal = self.dataset.get_signal(idx)
        if len(label)==0: #Implies that label might be empty (e.g. when no labels are available (for Inference))
            return signal, filename
        else:
            return signal, filename, label, mask


def create_dataloader(configObject):
    """Creates the dataloader handler

    Args:
        configObject ([Configuration]): [Object containing all backend configuration settings]
        Required configObject.args:
            - mode
            - dataset
            - trainingDataPath, validationDataPath and/or testingDataPath
            - batchSize
            - usePatches
            - useSimulatedData
            - runValidation
            
    Returns:
        [type: List]: List of DataLoader objects. If mode=='TRAINING', it returns 
                      [trainingDataloader, validationDataloader]. If mode=='TESTING', it returns 
                      [testingDataloader]
    """
    
    if configObject.args.core.mode == 'TRAINING':
        training_path = Path(configObject.args.engine.trainingDataPath).absolute()
        validation_path = Path(configObject.args.engine.validationDataPath).absolute()

        training_database_handler = SampleDatabase(configObject, path=training_path)
        trainingDataloader = DataLoader(training_database_handler,
                                batch_size = 1 if configObject.args.task.usePatches else configObject.args.engine.batchSize,
                                shuffle = False,
                                num_workers = 0
                                )
        if not configObject.args.task.useSimulatedData and configObject.args.engine.runValidation:
            validation_database_handler = SampleDatabase(configObject, path=validation_path)
            validationDataloader = DataLoader(validation_database_handler,
                                    batch_size = 1 if configObject.args.task.usePatches else configObject.args.engine.batchSize,
                                    shuffle = False,
                                    num_workers = 0
                                    )
            dataloader = trainingDataloader, validationDataloader
        else:
            dataloader = trainingDataloader

    elif configObject.args.core.mode == 'TESTING':
        testing_path = Path(configObject.args.engine.testingDataPath).absolute()
        testing_database_handler = SampleDatabase(configObject, path=testing_path)
        testingDataloader = DataLoader(testing_database_handler,
                                batch_size = 1,
                                shuffle = False,
                                num_workers = 0
                                )
        dataloader = testingDataloader

    return dataloader
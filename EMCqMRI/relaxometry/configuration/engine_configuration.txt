{ "Configuration-file": {
    "training-and-inference":{ 
        "inferenceModel": "rim",
        "optimizer": "ADAM",
        "learningRate": 0.00505,
        "batchSize": 1,
        "usePatchesAsBatches": true,
        "epochs": 200,
        "lossFunction": "MSE",
        "loadCheckpoint": false,
        "loadCheckpointPath": "trained_models/resnet/t2/resnet_epoch_200_resnet_T2_Larger.pth"
    },
    

    "dataset-parameters":{
        "fileExtension": "rawb",
        "trainingDataPath": "relaxometry/data/training/",
        "testingDataPath": "relaxometry/data/testing/",
        "runValidation": false,
        "validationDataPath": ""
    },

    "save":{
        "saveResults": false,
        "saveResultsPath": "./",
        "saveCheckpoint": false,
        "saveCheckpointPath": "/data/scratch/esabidussi/data/Simulated/results_test",
        "sulfixCheckpoint": "rim_Large_motion_model_test1-batch24-lr001-psize40"
    },

    "system-parameters":{
        "useCUDA": true,
        "runBenchmark": true
    }
    }
} 

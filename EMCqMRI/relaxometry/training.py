from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import os
package_path = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, package_path)


import logging
from core.configuration import pll_configuration
from core.engine import build_model as core_build
from core.engine import train_model


def override_model(configObject):
    pass


if __name__=='__main__':
    configurationObj = pll_configuration.Configuration('TRAINING')
    override_model(configurationObj)
    core_build.make(configurationObj)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    logging.info('Starting training...')
    training = train_model.Train(configurationObj)
    training.execute()
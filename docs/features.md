# Feature highlights

## **Available Pre-implemented Models**

### Signal Models

- T1 Relaxometry: LookLocker
- T2 Relaxometry: Fast Spin Echo

### Log-Likelihood Models

- Gaussian Likelihood
- Rician Likelihood


### Inference Models

- Recurrent Inference Machines (**RIMs**)
- Residual Neural Network (**ResNet**)
- Maximum Likelihood Estimated (**MLE**)

### Dataset Models

- Relaxometry
- Motion Relaxometry

:github_url: https://gitlab.com/e.ribeirosabidussi/emcqmri

Inference Module
================
.. currentmodule:: EMCqMRI.core.base.base_inference_model.InferenceModel

.. autofunction:: forward
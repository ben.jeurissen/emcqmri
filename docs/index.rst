:gitlab_url: https://gitlab.com/e.ribeirosabidussi/emcqmri

.. EMCqMRI documentation master file, created by
   sphinx-quickstart on Wed Feb  5 09:40:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Project EMCqMRI
===============

EMCqMRI is a `PyTorch <https://pytorch.org/>`_-based, `open-source <https://gitlab.com/e.ribeirosabidussi/emcqmri/-/blob/master/LICENSE>`_ framework.

The EMCqMRI package was designed with the goal to accelerate initial developments in the field of quantitative MRI (qMRI).

It provides base classes for the implementation of task-specific custom modules and offers a framework for
training Deep Learning inference models and prediction of quantitative maps.

Validated qMRI methods, including signal models, likelihood functions and
inference methods, are also included within this package.

* Project status: Development


Getting started
---------------

*The codebase is currently under active development*

Main features:
- flexible creation of custom modules;
- compositional & portable APIs for ease of integration in existing workflows;
- domain-specific implementations for networks, losses, evaluation metrics and more;

The EMCqMRI package is distributed with PyPI. Details on environment preparation, PyTorch installation,
and external dependencies can be found in *Installation*.

Examples of projects using EMCqMRI:

- MR Relaxometry: `<https://gitlab.com/e.ribeirosabidussi/emcqmri_relaxometry>`_

Technical documentation is available at `emcqmri.readthedocs.io <https://emcqmri.readthedocs.io/en/latest/>`_.

.. toctree::
   :maxdepth: 1
   :caption: Features

   features.md

.. toctree::
   :maxdepth: 1
   :caption: APIs

   base_signal_model
   base_dataset
   base_inference_model
   base_likelihood_model

.. toctree::
   :maxdepth: 1
   :caption: Installation

   installation.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


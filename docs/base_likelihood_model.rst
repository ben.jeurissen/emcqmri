:github_url: https://gitlab.com/e.ribeirosabidussi/emcqmri

Likelihood Module
=================
.. currentmodule:: EMCqMRI.core.base.base_likelihood_model.Likelihood
    
.. autofunction:: logLikelihood
.. autofunction:: gradients

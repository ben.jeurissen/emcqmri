# **EMCqMRI**


<br>

# **About**
The EMCqMRI is a PyTorch-based package designed with the goal to accelerate initial developments in the field of quantitative MRI (qMRI).

It provides the base classes for the implementation of task-specific custom modules and offers a framework for training Deep Learning inference models and prediction of quantitative maps.

Validated qMRI methods, including signal models, likelihood functions and inference methods, are already included within this package.

<br>

# **Installing**

### Preparing the development enviroment

- Create a virtual environment for the project: `python3 -m venv venv_example_project`

- Activate the virtual environment: `. venv_example_project/bin/activate`

- Upgrade pip and install wheel: `pip3 install --upgrade pip wheel` (this will make sure you have no errors and warnings during installations with pip)

### Installing PyTorch

- MacOS without CUDA support: `pip3 install torch torchvision`

- MacOS with CUDA support: follow instructions at this URL: https://github.com/pytorch/pytorch#from-source

- Linux with CUDA support: `pip3 install torch torchvision`

- Windows with CUDA support: `pip3 install torch==1.7.1 torchvision==0.8.2 -f https://download.pytorch.org/whl/torch_stable.html`

### Installing the EMCqMRI package with PIP

- After activating your virtual enviroment, run: `pip3 install -i https://test.pypi.org/simple/ --extra-index-url https://pypi.python.org/simple/ --upgrade EMCqMRI`.

### Validating the installation

Once installed, you can verify if everything is running correctly.
- Start a Python session in your terminal: `python3`
- Import the EMCqMRI package: `import EMCqMRI`
- Verify the package version: `print(EMCqMRI.__version__)`

<br>

# **Support**
The following configurations were used and proved to work. Please, let us know if you tested the packaged in a different OS, Python version or PyTorch version.
### &nbsp; OS
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![macOS](https://img.shields.io/badge/macOS%20Catalina-10.15.6-blue)
![macOS](https://img.shields.io/badge/macOS%20Mojave-10.14.6-blue)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Linux](https://img.shields.io/badge/Linux%20Ubuntu-20.04.1-green)
![Linux](https://img.shields.io/badge/Linux%20Ubuntu-19.10-green)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Windows](https://img.shields.io/badge/Windows%207_64b-SP1-orange)

### &nbsp; Python
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Python](https://img.shields.io/badge/Python-2.7-red) 
![Python](https://img.shields.io/badge/Python-3.6.5-red) 
![Python](https://img.shields.io/badge/Python-3.6.8-red) 
![Python](https://img.shields.io/badge/Python-3.7.1-red) 
![Python](https://img.shields.io/badge/Python-3.8.0-red) 

### &nbsp; PyTorch
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![Python](https://img.shields.io/badge/PyTorch-1.4-blueviolet) 
![Python](https://img.shields.io/badge/PyTorch-1.6-blueviolet) 
![Python](https://img.shields.io/badge/PyTorch-1.7.1-blueviolet) 

<br>

# **Projects using the EMCqMRI**

- https://gitlab.com/e.ribeirosabidussi/emcqmri_relaxometry
- https://gitlab.com/e.ribeirosabidussi/emcqmri_MFRelaxometry

<br>

# **Using the EMCqMRI in your project**

The documentation for the APIs and implemented modules can be found at: https://emcqmri.readthedocs.io/en/latest/
### **APIs**

- **Signal Model**: It provides an interface for the implementation of custom signal models for model-based inference of quantitative MRI parameters. Custom initialization of parameters of the signal model is also supported.

- **Likelihood Model**: This interface allows for the implementation of custom likelihood models for model-based inference methods. In many cases, this model is also referred to as the data consistency model.

- **Inference Model**: Interface for the implementation of custom inference models. The EMCqMRI supports model-drive, data-drive and hibrid (data/model-driven) methods.

- **Dataset Model**: Interface for the construction of custom dataset models. These might include the generation of simulated data, loading of existing training and testing datasets, pre-processing and data augmentation techniques, etc.


<br>

### **Using the APIs**

The creation of custom modules is straighforward using the EMCqMRI APIs. They allow for seamlessly integration of your application to the core engine of EMCqMRI.

#### **Creating new custom modules**

#### Custom Signal Models

All new implementations of Signal Models should inherit from the base class SignalModel. Two abstract methods should be implemented by you:

```python
from EMCqMRI.core.base.base_signal_model import SignalModel

class MyCustomSignalModel(SignalModel):
    def __init__(self):
        # Your implementation
    
    def forwardModel(self, kappa, *extra_args):
        # Implementation of the signal model, where 'kappa' is a list of the latent 
        # variables of the model and '*extra_args' denotes any observable variable.

        return [signal]

    def initializeParameters(self):
        # This abstract methods allows you to initialize all latent variables of the
        # custom signal model.

        return [kappa]
```

#### Custom likelihood models

All new implementations of Likelihood Models should inherit from the base class Likelihood.

```python
from EMCqMRI.core.base.base_likelihood_model import Likelihood

class MyLikelihoodModel(Likelihood):
    def __init__(self):
        # Your implementation

    def logLikelihood(self, signal, modeled_signal):
        # Implementation of the likelihood model, where 'signal' is the observed data 
        # (i.e. weighted MR image) and 'modeled_signal' is the simulated (or synthetic)
        # data constructed using a given 'signal model' and latent variables.

        return [scalar error]

    def gradients(self, signal, kappa, *extra_args):
        # The EMCqMRI package offers a basic implementation to automatically compute
        # the gradients of 'signal' with respect to the latent variable 'kappa'.
        # It uses the autodifferentiation functionality from PyTorch.
        # You can override this method for more complex (or explicit) gradients.
        # Any observable variable should be passed within '*extra_args'.

        return [gradients of signal w.r.t. kappa]

```

#### Custom inference models

All new implementations of Inference Models should inherit from the base class SignalModel. The forward pass of the method should be implemented:

```python
from EMCqMRI.core.base.base_inference_model import InferenceModel

class MyInferenceModel(InferenceModel):
    def __init__(self):
        # Your implementation

    def forward(self, inputs):
        # Abstract function that defines the forward pass of the inference model.
        # It supports conventional and Deep Learning methods. For iterative methods,
        # the steps must be implemented within this function.
        # The variable 'inputs' depends on the application. For example, it can be
        # the latent variables, the signal (i.e. weighted images), etc.

        return [inferred latent variables (i.e. kappa)]

```


#### Custom datasets

For each new application, custom routines for loading, creation, pre-processing and augmentation of the training, validation and testing data. The base class Dataset provides basic methods for indexing of files, setting the data path, batch-loading of files in a folder, loading of individual files and length of the dataset. For many applications, only requires the implementation of the abstract methods below.

```python
from EMCqMRI.core.base.base_dataset import Dataset

class MyDataset(Dataset):
    def __init__(self):
        # Your implementation

    def get_label(self, idx):
        # Fetching labels and masks from the data directory, where 'idx' denotes the file
        # index within the data folder. With exception of manual indexing needs, 'idx' is
        # automatically provided by the EMCqMRI.core.
        # It is also possible to implement routines to generate simulated labels.

        return [ground-truth data, mask (when available)]

    
    def get_signal(self, idx):
        # Fetching training signal from the data directory, where 'idx' denotes the file
        # index within the data folder. With exception of manual indexing needs, 'idx' is
        # automatically provided by the EMCqMRI.core
        # It is also possible to implement routines to generate simulated labels.

        return [training (signal) data]
```

<br>

### **Integrating Custom Modules in your project**

After creating your custom modules, you will need to integrate them into your project. Below, an example of a Python file used to train a network. Here, 3 custom modules are used: Motion_relaxometry, Motion_RIM and Motion_dataset. We use a standard, pre-implemented Gaussian likelihood function for this method. Note that we don't need to specify this likelihood within the code. The choice of method can be controlled via the configuration files.

```python
import logging
from EMCqMRI.core.configuration import pll_configuration
from EMCqMRI.core.engine import build_model
from EMCqMRI.core.engine import train_model


def override_model(configObject):
        ################################################################
        # SET MODELS. OVERRIDES CONFIGURATION FROM FILE
        from custom_dataset import motion_dataset
        configObject.args.engine.dataset = motion_dataset.MotionDataset(configObject.args)

        from custom_signal_model import motion_relaxometry
        configObject.args.engine.signal_model = motion_relaxometry.MotionRelaxometry(configObject.args)

        from custom_inference_model import motion_rim
        configObject.args.engine.inference_model = motion_rim.MotionRim(configObject.args)

if __name__ == "__main__":

    configurationObj = pll_configuration.Configuration('TRAINING')
    override_model(configurationObj)
    build_model.make(configurationObj)

    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    logging.info('Starting inference...')
    infer_qMap = train_model.Train(configurationObj)
    infer_qMap.execute()

```

Because the custom modules follow the guidelines of the base classes (i.e. base_dataset, base_inference_model), they are expected to work seamlessly with the EMCqMRI framework.

### **Configuration Files**

The EMCqMRI package aims to provide high flexibility to design new applications. The package is controlled by four configuration files, kept independent from each other to increase modularity for new projects:

- **pll_configuration**: This is the starting point of the EMCqMRI. This file indicates the path to all configuration files. In that way, prototyping can be really quick.

- **engine configuration**: The engine configuration file defines all configurations for the core engine of the EMCqMRI. Here you should define the number of training epochs (for learning algorithms), which optimizer to use (i.e. ADAM, GD), batch size, learning rate, etc...

- **task configuration**: This file should contain the configurations used to control which application will run in the EMCqMRI. Here you can choose any available (i.e. pre-implemented) signal models, likelihood functions and, additionally, any configuration that is used in your dataset class.

- **inference model configuration**: This file should define all configurations used to implement and define the inference model (i.e. number of channels in a given convolutional layer, number of optimization iterations, etc).

Because the EMCqMRI automatically parses your configuration file, addition of custom configuration variables is straightforward. For example, you can define a new configuration in the engine configuration file called "log_loss_curve = True". This option will be automatically assigned to the internal variable "configObject.args.engine.log_loss_curve".

Note that custom modules will always override any pre-implemented module. In that way, the option specified in the configuration file won't have any effect.

Finally, we also offer the possibility to override configurations via the command line when calling Python file. This is not an automatic process, and you should define proper Python files for that (see an example in the file: "EMCqMRI/configuration/engine/engine_configuration.py). Also note that, for this to work correctly, the Python configuration file should contain the same options as your '.txt' file and should be named indentically.

### **Available Pre-implemented Models**

#### Signal Models

- T1 Relaxometry: LookLocker
- T2 Relaxometry: Fast Spin Echo

#### Log-Likelihood Models

- Gaussian Likelihood
- Rician Likelihood


#### Inference Models

- Recurrent Inference Machines (**RIMs**)
- Residual Neural Network (**ResNet**)
- Maximum Likelihood Estimated (**MLE**)

#### Dataset Models

- Relaxometry
- Motion Relaxometry


<br>


# Contributing / Reporting issues
Please refer to the project's coding and documentation style for submitting patches and additions. In general, we follow the "fork-and-pull" Git workflow.

1. Fork the repo on GitHub
2. Clone the project to your own machine
3. Commit changes to your own branch
4. Push your work back up to your fork
5. Submit a Pull request so that we can review your changes

NOTE: Be sure to merge the latest from "upstream" before making a pull request!

<br>

# Requirements

The EMCqMRI package is based on **Python** and **PyTorch**.
External dependencies are automatically installed with the package.

<br>

# Known bugs

There might be differences when using the EMCqMRI with Windows, MacOS and Linux, due to different conventions in the Path style. Please report them by creating an issue on this project.
<br>

# License
[MIT](https://choosealicense.com/licenses/mit/)


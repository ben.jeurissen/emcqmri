h5py >=2.10.0
numpy >=1.19.0
scipy >=1.2.1
matplotlib >=3.3.2
PyQt5 >=5.15.1
progress >=1.5